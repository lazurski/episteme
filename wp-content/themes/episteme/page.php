<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

get_header(); ?>

		<div id="primary">
			<div id="content" role="main">

      <?php if (is_front_page()):
        $sticky = get_option( 'sticky_posts' );
        rsort( $sticky );
        query_posts(
          array(
            'post__in' => $sticky
          )
        );
        if (have_posts()): ?>
        <div class='sticky-posts'>
        <? while ( have_posts() ) : the_post(); ?>
          <div class="entry-content">
            <div class="entry-meta">
              <?php twentyeleven_posted_on(); ?>
            </div><!-- .entry-meta -->
            <h1>
              <a href="<?php the_permalink() ?>" title="<?php the_title(); ?>">
                <?php the_title(); ?>
              </a>
            </h1>
            <?php the_content(); ?>
            <?php wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'twentyeleven' ) . '</span>', 'after' => '</div>' ) ); ?>
          </div><!-- .entry-content -->
        <?php endwhile; wp_reset_query(); /* end of sticky posts loop */ ?>
        </div>
      <?php endif; /* if have sticky posts */ ?>
      <?php endif; /* if it is fron page */?>

				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'content', 'page' ); ?>

					<?php comments_template( '', true ); ?>

				<?php endwhile; // end of the loop. ?>

			</div><!-- #content -->
		</div><!-- #primary -->

<?php get_footer(); ?>