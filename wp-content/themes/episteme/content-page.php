<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-content">
		<?php the_content(); ?>
		<?php wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'twentyeleven' ) . '</span>', 'after' => '</div>' ) ); ?>
	</div><!-- .entry-content -->
	<?php
	$children_args = array(
	  'post_type' => array ('page'),
	  'child_of' => $post->ID,
	  'post_parent' => $post->ID
	);
	$children = new WP_Query($children_args);
	if ($children->have_posts()):
	?>
	<div class="child-entries">
		<?php while ($children->have_posts()):
		  $children->the_post(); global $more; $more = 0; ?>
		  <div class="entry-content">
		  	<h1>
		  		<a href="<?php the_permalink() ?>" title="<?php the_title(); ?>">
		  			<?php the_title(); ?>
		  		</a>
		  	</h1>
		  	<?php the_content(); ?>
		  	<?php wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'twentyeleven' ) . '</span>', 'after' => '</div>' ) ); ?>
		  </div><!-- .entry-content -->
		<?php endwhile; wp_reset_postdata(); /* while children have posts */?>
	</div>
	<?php endif; /* children have posts */ ?>
	<footer class="entry-meta">
		<?php edit_post_link( __( 'Edit', 'twentyeleven' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-meta -->
</article><!-- #post-<?php the_ID(); ?> -->
