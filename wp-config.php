<? 
/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');


@ini_set('log_errors','On');
@ini_set('display_errors','Off');
@ini_set('error_log', ABSPATH . '../php-error.log');

$sitecfg = ABSPATH . "../wp-config-site.php";
if (file_exists($sitecfg)) include_once ("$sitecfg");

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

?>